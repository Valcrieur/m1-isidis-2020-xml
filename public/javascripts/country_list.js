let search = "";
let continent = "";

async function display() {
    // TODO: load xml and xsl
    const xml = await get('../xml/countries.xml');
    const xsl = await get('../xsl/country_list.xsl');

    // TODO: xsl transformations
    const proc = new XSLTProcessor();
    proc.importStylesheet(xsl);
	proc.setParameter(null, 'search', search);
	proc.setParameter(null, 'continent', continent);
    const result = proc.transformToFragment(xml, document);
    
    // TODO: append in #frag_country_list	
	document.querySelector('#frag_country_list').appendChild(result);
}

document.getElementById("search_name").onkeyup = async function(){
	search = this.value;
	
	document.querySelector('#frag_country_list').innerHTML = "";
	display();
}

document.getElementById("search_continent").onchange = async function(){
	continent = this.value;
	
	document.querySelector('#frag_country_list').innerHTML = "";
	display();
}

display();
