async function sayHello() {
    const xml = await get('../xml/countries.xml');    
    const country = new URL(location).searchParams.get('country');
    var nsResolver = xml.createNSResolver( xml.ownerDocument == null ? xml.documentElement : xml.ownerDocument.documentElement);
    const cc = xml.evaluate("//country[name='"+country+"']/name/@cc", xml, nsResolver, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.textContent;

    const json = await get("https://fourtonfish.com/hellosalut/?cc=" + cc, 'json');
    const greetings = JSON.parse(json);
    document.querySelector('#say_hello').innerHTML = greetings.hello;
}

sayHello();
