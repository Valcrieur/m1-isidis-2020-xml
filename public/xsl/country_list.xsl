<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
                
<xsl:param name="search" />
<xsl:param name="continent" />

<xsl:template match="/">
  <table class="table">
    <thead class="thead-light">
      <tr>
        <th>#</th>
        <th>Country</th>
        <th>Continent</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="//country[contains(name, $search) and ($continent = '' or @continent = $continent)]">
        <xsl:sort select="name" order="ascending"/>
        <tr>
          <td><img src="{flag/@src}" width="50px"/></td>
          <td><a href="/html/country_infos.html?country={name}"><xsl:value-of select="name"/></a></td>
          <td><xsl:value-of select="@continent"/></td>
        </tr>
	    </xsl:for-each>
    </tbody>
  </table>
</xsl:template>

</xsl:stylesheet>
