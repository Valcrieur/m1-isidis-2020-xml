<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:param name="country" />

<xsl:template match="/">
  <h1>
    <xsl:value-of select="//country[name=$country]/name"/>&#160;
    <span class="badge badge-light text-uppercase"><xsl:value-of select="//country[name=$country]/name/@cc"/></span>
  </h1>
  <hr/>
  <div class="container">
    <div class="row">
        <div class="col pl-0">
            <img src="{//country[name=$country]/flag/@src}" width="100%"/>
        </div>
        <div class="col pr-0">
            <p><span class="font-weight-bold">Capital city: </span><xsl:value-of select="//country[name=$country]/capital"/></p>
            <p><span class="font-weight-bold">Population: </span><xsl:value-of select="//country[name=$country]/population"/> inhabitants</p>
            <p><span class="font-weight-bold">Area: </span><xsl:value-of select="//country[name=$country]/area"/> km²</p>
        </div>
    </div>
  </div>
  <hr/>
  <h1>Location on Earth</h1>
  <iframe
    width="100%"
    height="400"
    frameborder="0"
    marginheight="0"
    marginwidth="0"
    src="https://maps.google.com/maps?q={//country[name=$country]/name}&amp;hl=en&amp;output=embed">
  </iframe>
</xsl:template>

</xsl:stylesheet>
